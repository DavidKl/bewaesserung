#!/bin/bash
#Inbetriebnahme 
#Team 14: Simon Weigand, Lukas Reinhardt, David Klemm

#Abkuerzungen
INSTALL="sudo apt-get install -y "
UPDATE="sudo apt-get update"
cd ..

#Zeitzone anpassen
#sudo ln -fs ~/usr/share/zoneinfo/Europe/Berlin /etc/localtime
#sudo dpkg-reconfigure -f noninteractive tzdata

#System aktualisieren
$UPDATE
sudo apt-get upgrade

#Apache2
$INSTALL apache2

#Rechte fuer html Ordner anpassen
sudo chmod 775 -c -R /var/www/html

#Webbrowser einfuegen Apache2
cd
cd bewaesserung/
cp wasser.php /var/www/html
cp data.php /var/www/html
cp style.css /var/www/html
cp insertAuto.php /var/www/html
cp insertManuel.php /var/www/html
cp insertMail.php /var/www/html
cp deleteEMail.php /var/www/html
cp logo.PNG /var/www/html
cp setWinter.php /var/www/html
cp autoTimes.php /var/www/html
cp config.php /var/www/html
cp readEMail.php /var/www/html
cp readModus.php /var/www/html
cp readZustand1.php /var/www/html
cp readZustand2.php /var/www/html
cp readZustand3.php /var/www/html
cd

#restart Apache2
sudo systemctl restart apache2

#Python-pip installieren
$INSTALL python-pip
$INSTALL python3-pip
pip3 install mysql-connector
pip install python-crontab

#SENSE-HAT installieren
$INSTALL sense-hat

#php-Pakete
$INSTALL php libapache2-mod-php php-mysql 

#MYSQL Datenbank
$INSTALL mariadb-server
$INSTALL phpmyadmin

#System aktualisieren
$UPDATE
sudo apt-get upgrade


sudo mysql --user=root mysql <<MYSQL_SCRIPT


CREATE USER 'bewaesserer'@'localhost' IDENTIFIED BY 'raspberry';
GRANT ALL PRIVILEGES ON *.* TO 'bewaesserer'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;

CREATE DATABASE Bewaesserung;
USE Bewaesserung;

SET time_zone = '+0:00';
SET GLOBAL time_zone = '+0:00';

CREATE TABLE Bewaesserung.Mailempfaenger(ID INT NOT NULL Auto_INCREMENT, Emailadresse VARCHAR(255), PRIMARY KEY (ID));
CREATE TABLE Bewaesserung.Tempverlauf(Zeit DATETIME, Temperatur FLOAT, PRIMARY KEY (Zeit));
CREATE TABLE Bewaesserung.Temperatur(ID INT NOT NULL Auto_INCREMENT, Temp FLOAT, PRIMARY KEY (ID));
CREATE TABLE Bewaesserung.Modus(ID INT NOT NULL Auto_INCREMENT, Modus VARCHAR(255), PRIMARY KEY (ID));
CREATE TABLE Bewaesserung.Bereich(Bereich INT, Einstellung VARCHAR(255), Zustand VARCHAR(255), Start1 VARCHAR(255), Ende1 Varchar(255), Start2 VARCHAR(255), Ende2 VARCHAR(255), PRIMARY KEY (Bereich));

INSERT INTO Modus VALUES ('1', 'Winter');
INSERT INTO Bereich VALUES ('1', 'off', 'off', '', '', '', '');
INSERT INTO Bereich VALUES ('2', 'off', 'off', '', '', '', '');
INSERT INTO Bereich VALUES ('3', 'off', 'off', '', '', '', '');
INSERT INTO Temperatur VALUES ('1', '0');

MYSQL_SCRIPT




#System aktualisieren
$UPDATE
sudo apt-get upgrade

