    <!DOCTYPE html>

    <html>
    <head>
    <title>Table with times</title>
    <style>
    table {
    border-collapse: collapse;
    width: 100%;
    color: #588c7e;
    font-family: monospace;
    font-size: 25px;
    text-align: left;
    }
    th {
    background-color: #588c7e;
    color: white;
    }
    tr:nth-child(even) {background-color: #f2f2f2}
    </style>
    </head>
    <body>
    <table>
    <tr>
    <th>Bereich</th>
    <th>Start1</th>
    <th>Ende1</th>
    <th>Start2</th>
    <th>Ende2</th>
    </tr>
    <?php
    $conn = mysqli_connect("localhost", "bewaesserer", "raspberry", "Bewaesserung");
    // Check connection
    if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT Bereich, Start1, Ende1, Start2, Ende2 FROM Bereich";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
    echo "<tr><td>" . $row["Bereich"]. "</td><td>" . $row["Start1"] . "</td><td>"
    . $row["Ende1"]. "</td><td>" . $row["Start2"] . "</td><td>" . $row["Ende2"] . "</td></tr>";
    }
    echo "</table>";
    } else { echo "0 results"; }
    $conn->close();
    ?>
    </table>
    </body>
    </html>
