<?php
		include "config.php";

		$area1start1 = filter_input(INPUT_POST, 'area1time1start');
		$area1finish1 = filter_input(INPUT_POST, 'area1time1end');
		$area1start2 = filter_input(INPUT_POST, 'area1time2start');
		$area1finish2 = filter_input(INPUT_POST, 'area1time2end');
		$area2start1 = filter_input(INPUT_POST, 'area2time1start');
		$area2finish1 = filter_input(INPUT_POST, 'area2time1end');
		$area2start2 = filter_input(INPUT_POST, 'area2time2start');
		$area2finish2 = filter_input(INPUT_POST, 'area2time2end');
		$area3start1 = filter_input(INPUT_POST, 'area3time1start');
		$area3finish1 = filter_input(INPUT_POST, 'area3time1end');
		$area3start2 = filter_input(INPUT_POST, 'area3time2start');
		$area3finish2 = filter_input(INPUT_POST, 'area3time2end');
		
		$stateArea1 = filter_input(INPUT_POST, 'stateArea1');
		$stateArea2 = filter_input(INPUT_POST, 'stateArea2');
		$stateArea3 = filter_input(INPUT_POST, 'stateArea3');
		
		//falls checkboxen nicht gesetzt, off
		if($stateArea1 == "")
		{
			$stateArea1 = "off";
		}
		
		if($stateArea2 == "")
		{
			$stateArea2 = "off";
		}
		
		if($stateArea3 == "")
		{
			$stateArea3 = "off";
		}
		
		//Zeiten speichern
		$sql = "UPDATE Bereich 
				SET Start1 = '$area1start1', Ende1 = '$area1finish1', Start2 = '$area1start2', Ende2 = '$area1finish2'
				WHERE Bereich = '1'";
		$conn->query($sql);
		
		$sql = "UPDATE Bereich
					SET Start1 = '$area2start1', Ende1 = '$area2finish1', Start2 = '$area2start2', Ende2 = '$area2finish2'
					WHERE Bereich = '2'";
		$conn->query($sql);

		$sql = "UPDATE Bereich 
					SET Start1 = '$area3start1', Ende1 = '$area3finish1', Start2 = '$area3start2', Ende2 = '$area3finish2'
					WHERE Bereich = '3'";
		$conn->query($sql);
		
		//speichern ob bereich ein oder ausgeschaltet 
		$sql = "UPDATE Bereich SET Einstellung = '$stateArea1' WHERE Bereich = '1'";
		$conn->query($sql);
		
		$sql = "UPDATE Bereich SET Einstellung = '$stateArea2' WHERE Bereich = '2'";
		$conn->query($sql);
		
		$sql = "UPDATE Bereich SET Einstellung = '$stateArea3' WHERE Bereich = '3'";
		$conn->query($sql);
		
		//Bereiche zum start alle auf off setzten
		$sql = "UPDATE Bereich SET Zustand = 'off'";
		$conn->query($sql);
		
		//Modus speichern
		$sql = "UPDATE Modus SET Modus = 'Automatik' WHERE ID = '1'";
		$conn->query($sql);
		
		$conn->close();
?>

