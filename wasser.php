<!DOCTYPE html>
<html lang="en" dir="ltr">

	<head>
		<meta charset="utf-8">
		<title>Smart Watering</title>
		<link rel="stylesheet" type="text/css" href="style.css" />
		<link href="database.php"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>

	<body>

			<!-- Logo einfügen -->
			<img id="picture" src="logo.PNG" alt="Bild nicht gefunden!" width="100" height="100">
			
			<!-- Titel einfügen -->
			<h1 id="headline">Smart Watering V 1.1.0</h1>

			<div id="date">
				<!--aktuelles Datum erzeugen-->
				<script>
					var datum = new Date();
					var day = datum.getDate();
					var month = datum.getMonth()+1;
					var year = datum.getYear()+1900;
					document.write(day + "." + month+ "." + year);
				</script>
				<label><br>Team 14</label>
			</div>

		<!--Buttons zur Auswahl des Modus-->
		<div id="modusButtons">
			<button type="button" class="modusButtons" id="buttonAutomatik"><label id="labelAuto">Automatik</label></button>
			<button type="button" class="modusButtons" id="buttonManuel"><label id="labelManuel"> Manuel</label></button>
			<button type="button" class="modusButtons" id="buttonWinter"><label id="labelWinter">Winter</label></button>
		</div>
			
		<!--Zeiten für Automatikmodus-->
		<div class="settingsAutomatik" id="settingsAutomatik">

			<form id="formAuto">

				<div id="auto1">
					<h2>Zeitraum 1</h2>
					<!-- Zeiten für erste Bewässerung -->
					<ol id="listAuto1">
						<li><label>Bereich 1: <input type="time" name="area1time1start"></label> - <input type="time" name="area1time1end"></label></li>
						<li><label>Bereich 2: <input type="time" name="area2time1start"></label> - <input type="time" name="area2time1end"></label></li>
						<li><label>Bereich 3: <input type="time" name="area3time1start"></label> - <input type="time" name="area3time1end"></label></li>
					</ol>
				</div>

				<div id="auto2">
					<h2>Zeitraum 2</h2>
					<!-- Zeiten für zweite Bewässerung -->
					<ol id="listAuto2">
						<li><label>Bereich 1: <input type="time" name="area1time2start"></label> - <input type="time" name="area1time2end"></label> <input type="checkbox" id="toggle1" class="offscreen" name="stateArea1"/> <label for="toggle4" class="switch"></label></li>
						<li><label>Bereich 2: <input type="time" name="area2time2start"></label> - <input type="time" name="area2time2end"></label> <input type="checkbox" id="toggle2" class="offscreen" name="stateArea2"/> <label for="toggle5" class="switch"></label></li>
						<li><label>Bereich 3: <input type="time" name="area3time2start"></label> - <input type="time" name="area3time2end"></label> <input type="checkbox" id="toggle3" class="offscreen" name="stateArea3"/> <label for="toggle6" class="switch"></label></li>
					</ol>
				</div>
				<!-- Button zum abschicken der Daten an PHP Skript -->
				<label><button type="button" name="save" id="buttonSaveAutomatik">Speichern</button></label>
			</form>
			
			<label id="linkTimes"><a href="autoTimes.php" target="blank">Zeiten</a></label>	
		</div>

		<!--Einstellungen für Manuellen Modus-->
		<div class="settingsManuel" id="settingsManuel">
			<form id="formManuel">
				<!-- Boxen für manuellen Modus -->
				<ol id="listManuel">
					<li>Bereich 1: <input type="checkbox" id="toggle7" class="offscreen" name="boxArea1" /> <label for="toggle4" class="switch"></label></li>
					<li>Bereich 2: <input type="checkbox" id="toggle8" class="offscreen" name="boxArea2" /> <label for="toggle5" class="switch"></label></li>
					<li>Bereich 3: <input type="checkbox" id="toggle9" class="offscreen" name="boxArea3" /> <label for="toggle6" class="switch"></label></li>
				</ol>
				<label><button type="button" id="buttonSaveManuel">Speichern</button></label>
			</form>
		</div>

		<!-- Ausgabe Winter Modus -->
		<div class="settingsWinter" id="modusWinter">
			Alle Bereiche deaktiviert!
		</div>

		<!-- "Dashboard" zum Anzeigen des aktuellen Status -->
		<div class="areas" id="areas">
			<div id="area1">
				Bereich 1
			</div>
			<div id="area2">
				Bereich 2
			</div>
			<div id="area3">
				Bereich 3
			</div>
		</div>

		<!-- Buttons zum hinzufügen und löschen von Emails -->
		<div class="eAdress" id="eAdress">
			<form id="formEMail">
				<label for="vname">E-Mail Adresse:
					<input type="email" name="email"><button type="button" id="buttonEAdress" class="btnEAdress">Hinzufügen</button>
					<button type="button" id="deleteEmail">Löschen</button>
				</label>
			</form>
		</div>
		
		<!-- gespeicherte Email Adressen anzeigen -->
		<div id="table">
			
		</div>	
		
		<div id="linkTemp">
			<!-- Link zu Temperaturwerten -->
			<p>
				<a href="data.php" target="blank">Hier</a> geht es zu den Temperaturwerten.
			</p>
		</div>
		
			<script type="text/javascript">

			<!-- Modi beim start verbergen -->
			$("#settingsAutomatik").hide();
			$("#settingsManuel").hide();
			$("#modusWinter").hide();
			
			<!--sobald Dokument fertig geladen-->
			$(document).ready(function() {

				<!-- nach click auf einen Button die entsprechenden Einstellungen anzeigen -->
				$("#buttonAutomatik").click(function() {
					$("#settingsAutomatik").show();
					$("#settingsManuel").hide();
					$("#modusWinter").hide();
				});

				$("#buttonManuel").click(function() {
					$("#settingsAutomatik").hide();
					$("#settingsManuel").show();
					$("#modusWinter").hide();
				});

				$("#buttonWinter").click(function() {
					$("#settingsAutomatik").hide();
					$("#settingsManuel").hide();
					$("#modusWinter").show();
					
					<!-- alle Bereiche auf "off" setzten" -->
					$.ajax({
							url: 'setWinter.php',
						});
						
					alert ("Sie haben in der Winter Modus gewechselt.");
				});
				
				$("#buttonEAdress").click(function() {	
					<!-- Skript zum einfügen in DB aufrufen -->
					$.ajax({
							url:"insertMail.php",
							type:"post",
							data:$("#formEMail").serialize(),
						});
				});
				
				$("#deleteEmail").click(function() {
					<!-- Bestätigungsfenster zum Löschen der Email Adressen-->
					if(confirm("Wollen Sie wirklich ALLE E-Mail Adressen löschen?"))
					{
						$.ajax({
							url: 'deleteEMail.php',
						});
					}	
				});
				
				$("#buttonSaveAutomatik").click(function() {	
					$.ajax({
							url:"insertAuto.php",
							type:"post",
							data:$("#formAuto").serialize(),
							success: function(){
									alert ("Einstellungen gespeichert.");
								},
						}); 
				});
				
				$("#buttonSaveManuel").click(function() {	
					$.ajax({
							url:"insertManuel.php",
							type:"post",
							data:$("#formManuel").serialize(),
						});
				});
				
				<!-- Einstellungen beim Start anzeigen -->
				var inhaltModus = $.get("readModus.php", function(data){
					var ausgabeModus = JSON.stringify(inhaltModus);
					modus = ausgabeModus.substr(ausgabeModus.indexOf("modus")+10, 3);
					
					if(modus == "Aut")
					{				
						<!-- Einstellungsmöglchkeiten für Automatikbetrieb anzeigen -->
						$("#settingsAutomatik").show();
					}
					
					if(modus == "Man")
					{				
						<!-- Einstellungsmöglchkeiten für Manuelbetrieb anzeigen -->
						$("#settingsManuel").show();
					}
					
					if(modus == "Win")
					{
						<!-- Einstellungsmöglchkeiten für Winterbetrieb anzeigen -->
						$("#modusWinter").show();
					}
				});
				
				<!-- im Intervall von 1 sekunde verschiedene DB Einträge auslesen -->
				setInterval(function(){
					<!-- Email Adressen aus Datenbank auslesen und anzeigen -->
					$("#table").load("readEMail.php");
					
					<!-- Zustand von Bereich 1 einlesen -->
					var inhalt1 = $.get("readZustand1.php", function(data){
						var ausgabe1 = JSON.stringify(inhalt1);
						zustand1 = ausgabe1.substr(ausgabe1.indexOf("zustand")+12, 2);
						<!-- Falls "on" hintergrundfarbe auf blau ändern -->
						if(zustand1 == "on"){
							document.getElementById('area1').style.backgroundColor = "blue";
						}else{
							document.getElementById('area1').style.backgroundColor = "red";
						}
					});
					
					<!-- Zustand von Bereich 2 einlesen -->
					var inhalt2 = $.get("readZustand2.php", function(data){
						var ausgabe2 = JSON.stringify(inhalt2);
						zustand2 = ausgabe2.substr(ausgabe2.indexOf("zustand")+12, 2);
						<!-- Falls "on" hintergrundfarbe auf blau ändern -->
						if(zustand2 == "on"){
							document.getElementById('area2').style.backgroundColor = "blue";
						}else{
							document.getElementById('area2').style.backgroundColor = "red";
						}
					});
					
					<!-- Zustand von Bereich 3 einlesen -->
					var inhalt3 = $.get("readZustand3.php", function(data){
						var ausgabe3 = JSON.stringify(inhalt3);
						zustand3 = ausgabe3.substr(ausgabe3.indexOf("zustand")+12, 2);
						<!-- Falls "on" hintergrundfarbe auf blau ändern -->
						if(zustand3 == "on"){
							document.getElementById('area3').style.backgroundColor = "blue";
						}else{
							document.getElementById('area3').style.backgroundColor = "red";
						}
					});
					
				<!-- Aktuellen Modus anzeigen -->
				var inhaltModus = $.get("readModus.php", function(data){
					var ausgabeModus = JSON.stringify(inhaltModus);
					modus = ausgabeModus.substr(ausgabeModus.indexOf("modus")+10, 3);
					
					if(modus == "Aut")
					{
						<!-- Button "Automatik" als "selected" festlegen -->
						$('#buttonAutomatik').addClass('selected');
						$('#buttonManuel').removeClass('selected');
						$('#buttonWinter').removeClass('selected');
					}
					
					if(modus == "Man")
					{
						<!-- Button "Manuel" als "selected" festlegen -->
						$('#buttonAutomatik').removeClass('selected');
						$('#buttonManuel').addClass('selected');
						$('#buttonWinter').removeClass('selected');
					}
					
					if(modus == "Win")
					{
						<!-- Button "Winter" als "selected" festlegen -->
						$('#buttonAutomatik').removeClass('selected');
						$('#buttonManuel').removeClass('selected');
						$('#buttonWinter').addClass('selected');
					}
				});
				}, 1000);
			});
		</script>
		
	</body>

</html>
