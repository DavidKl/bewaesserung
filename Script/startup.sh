#!/bin/bash
#Inbetriebnahme 
#Team 14: Simon Weigand, Lukas Reinhardt, David Klemm

#Abkuerzungen
INSTALL="sudo apt-get install -y "
UPDATE="sudo apt-get update"

#Zeitzone anpassen
sudo ln -fs ~/usr/share/zoneinfo/Europe/Berlin /etc/localtime
sudo dpkg-reconfigure -f noninteractive tzdata

#System aktualisieren
$UPDATE
sudo apt-get upgrade

#Apache2
$INSTALL apache2

#Rechte fuer html Ordner anpassen
sudo chmod 777 -c -R /var/www/html

#Webbrowser einfuegen Apache2
cd ..
cd bewaesserung/
cp wasser.php /var/www/html
cp data.php /var/www/html
cp style.css /var/www/html
cp logo.PNG /var/www/html
cd ..

#restart Apache2
sudo systemctl restart apache2

#Python-pip installieren
$INSTALL python-pip
pip install python-crontab

#php-Pakete
$INSTALL php libapache2-mod-php php-mysql 

#MYSQL Datenbank
$INSTALL mysql-server
$INSTALL phpmyadmin

#System aktualisieren
$UPDATE
sudo apt-get upgrade


sudo mysql --user=root mysql <<MYSQL_SCRIPT


CREATE USER 'bewaesserer'@'localhost' IDENTIFIED BY 'raspberry';
GRANT ALL PRIVILEGES ON *.* TO 'bewaesserer'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;

CREATE DATABASE Bewaesserung;
USE Bewaesserung;

SET time_zone = '+0:00';
SET GLOBAL time_zone = '+0:00';

CREATE TABLE Bewaesserung.Mailempfaenger(ID INT NOT NULL Auto_INCREMENT, Emailadresse VARCHAR(255), PRIMARY KEY (ID));
CREATE TABLE Bewaesserung.Tempverlauf(Zeit DATETIME, Temperatur FLOAT, PRIMARY KEY (Zeit));
CREATE TABLE Bewaesserung.Modus(ID INT NOT NULL Auto_INCREMENT, Modus VARCHAR(255), PRIMARY KEY (ID));
CREATE TABLE Bewaesserung.Bereich(Bereich INT, Einstellung VARCHAR(255), Zustand VARCHAR(255), Start1 VARCHAR(255), Ende1 Varchar(255), Start2 VARCHAR(255), Ende2 VARCHAR(255), PRIMARY KEY (Bereich));

INSERT INTO Modus VALUES ('', 'Winter');
INSERT INTO Bereich VALUES ('1', 'off', 'off', '', '', '', '');
INSERT INTO Bereich VALUES ('2', 'off', 'off', '', '', '', '');
INSERT INTO Bereich VALUES ('3', 'off', 'off', '', '', '', '');

MYSQL_SCRIPT




#System aktualisieren
$UPDATE
sudo apt-get upgrade

