import DBConnection
import SendMail
import datetime
import SenseHat
import ledset


#region variables
DB = DBConnection.DBConnect()
EmailTextWinter = "Die Bewaesserungsanlage ist in den Wintermodus gewechselt, \n da die Temperatur unter 5°C gefallen ist"
EmailTextAutomatik = "Die Bewaesserungsanlage ist in den Automatikmodus gewechselt, \n da die Temperatur über 15°C erreicht hat \n Der Automatik Modus wird entsprechend der zuletzt gespeicherten Daten ausgeführt"
date = datetime.datetime.now().time()
ActTime = str(date)
ActTime = ActTime[0:5]

blue=(0,0,255)
red=(255,0,0)

TEMP = SenseHat.GetTemp()
Command = "Update Temperatur set Temp = '%s' where ID = 1"%(TEMP)
DB.WriteData(Command)
SenseHat.msg(str(round(TEMP,2)),blue)

# Lists for data from the database
EmailList = []
ModusList = []
BereichList = []
Start1List = []
Ende1List = []
Start2List = []
Ende2List = []
EinstellungList = []
ZustandList = []
#endregion

#region SQL commands
EmailSelect = "SELECT Emailadresse from Mailempfaenger"
ModusSelect = "SELECT Modus from Modus"
BereichSelect = "SELECT Bereich From Bereich"
Start1Select = "SELECT Start1 From Bereich"
Ende1Select = "SELECT Ende1 From Bereich"
Start2Select = "SELECT Start2 From Bereich"
Ende2Select = "SELECT Ende2 From Bereich"
EinstellungSelect = "SELECT Einstellung From Bereich"
ZustandSelect = "SELECT Zustand From Bereich"
#endregion

# get data from database
def GetList(sqlresult,resultlist):
    index = 0
    for data in sqlresult:
        resultlist.insert(index,data[0])
        index += 1

#region Create Lists from Database
EmailResult = DB.ReadData(EmailSelect)
GetList(EmailResult,EmailList)

ModusResult = DB.ReadData(ModusSelect)
GetList(ModusResult,ModusList)

BereichResult = DB.ReadData(BereichSelect)
GetList(BereichResult,BereichList)

if(ModusList[0] == "Automatik"):
    Start1Result = DB.ReadData(Start1Select)
    GetList(Start1Result,Start1List)

    Ende1Result = DB.ReadData(Ende1Select)
    GetList(Ende1Result,Ende1List)

    Start2Result = DB.ReadData(Start2Select)
    GetList(Start2Result,Start2List)

    Ende2Result = DB.ReadData(Ende2Select)
    GetList(Ende2Result,Ende2List)

EinstellungResult = DB.ReadData(EinstellungSelect)
GetList(EinstellungResult,EinstellungList)

ZustandResult = DB.ReadData(ZustandSelect)
GetList(ZustandResult,ZustandList)
#endregion

# called when an area is irrigated
def SystemOn(time1,area):
    if(time1 == ActTime):
        Command= "Update Bereich set Zustand = 'on' where Bereich = %s" % (area)
        DB.WriteData(Command)

#called when the irrigiation is finished
def SystemOff(time1,area):
    if(time1 == ActTime):
        Command= "Update Bereich set Zustand = 'off' where Bereich = %s" % (area)
        DB.WriteData(Command)

# watering system on automatically 1
if(ModusList[0] == "Automatik"):
    for i in range(0,len(BereichList),1):
        if(EinstellungList[i] == "on"):
            SystemOn(Start1List[i],BereichList[i])

# watering system finished ,modus=automatic 1
if(ModusList[0] == "Automatik"):
    for i in range(0,len(BereichList),1):
        if(EinstellungList[i] == "on"):
            SystemOff(Ende1List[i],BereichList[i])

# watering system on automatically 2
if(ModusList[0] == "Automatik"):
    for i in range(0,len(BereichList),1):
        if(EinstellungList[i] == "on"):
            SystemOn(Start2List[i],BereichList[i])

# watering system finished ,modus=automatic 2
if(ModusList[0] == "Automatik"):
    for i in range(0,len(BereichList),1):
        if(EinstellungList[i] == "on"):
            SystemOff(Ende2List[i],BereichList[i])
            
# Modus changed, show on display
if (ModusList[0] != "Winter" and TEMP <= 5):
    Command = "Update Modus set Modus = 'Winter' where ID = 1"
    DB.WriteData(Command)
    SenseHat.msg("Winter",blue)
    
if (ModusList[0] == "Winter" and TEMP >= 15):
    Command = "Update Modus set Modus = 'Automatik' where ID = 1"
    DB.WriteData(Command)
    SenseHat.msg("Automatik",red)

for i in range(0,len(EmailList),1):
    EmpfaengerEmail = EmailList[i]
    #if actual Modus!= Winter and temperature below -5 --> modus changed to Winter
    if (ModusList[0] != "Winter" and TEMP <= 5):
        Command= "Update Modus set Modus = 'Winter' where ID = 1"
        DB.WriteData(Command)
        SendMail.MailSend(EmpfaengerEmail,EmailTextWinter)

    #if actual Modus== Winter and temperature above 15 --> modus changed to Automatik
    if (ModusList[0] == "Winter" and TEMP >= 15):
        Command= "Update Modus set Modus = 'Automatik' where ID = 1"
        DB.WriteData(Command)
        SendMail.MailSend(EmpfaengerEmail,EmailTextAutomatik)

#set Led and get ZustandList after Update
ZustandResult = DB.ReadData(ZustandSelect)
GetList(ZustandResult,ZustandList)
ledset.led(ZustandList)
