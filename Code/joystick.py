from sense_hat import SenseHat
sense = SenseHat()
sense.clear()

while True:

  for event in sense.stick.get_events():

    # Check if the joystick was pressed

    if event.action == "pressed":
        
      # Check which direction

      if event.direction == "up":

        #Change mode to Winter

      elif event.direction == "down":
        #not used

      elif event.direction == "left": 
        #Change mode to Automatik

      elif event.direction == "right":
        #Change mode to Manuell

      elif event.direction == "middle":
        #not used

      sense.clear()
