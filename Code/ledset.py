import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

def led(ZustandList):
    #Set Led 1
    if(ZustandList[0] == "on"):
        GPIO.setup(5,GPIO.OUT)
        GPIO.output(5,GPIO.HIGH)
    if(ZustandList[0] == "off"):
        GPIO.setup(5,GPIO.OUT)
        GPIO.output(5,GPIO.LOW)
    
    #Set Led 2
    if(ZustandList[1] == "on"):
        GPIO.setup(6,GPIO.OUT)
        GPIO.output(6,GPIO.HIGH)
    if(ZustandList[1] == "off"):
        GPIO.setup(6,GPIO.OUT)
        GPIO.output(6,GPIO.LOW)
    
    #Set Led 3  
    if(ZustandList[2] == "on"):
        GPIO.setup(13,GPIO.OUT)
        GPIO.output(13,GPIO.HIGH)
    if(ZustandList[2] == "off"):
        GPIO.setup(13,GPIO.OUT)
        GPIO.output(13,GPIO.LOW)