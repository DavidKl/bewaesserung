import DBConnection
import datetime

DB = DBConnection.DBConnect()
d = datetime.datetime.now()

#get date as 2020-01-12 12:00
date = str(d)
date = date[0:19]
date = "'%s'" %(date)
TempSelect = "SELECT Temp from Temperatur where ID = 1"
TempList = []
def GetList(sqlresult,resultlist):
    index = 0 
    for data in sqlresult:
        resultlist.insert(index,data[0]) 
        index += 1
        
#region Create Lists from Database
TempResult = DB.ReadData(TempSelect)
GetList(TempResult,TempList)
TEMP = TempList[0]
Command = "INSERT INTO Tempverlauf (Zeit,Temperatur) VALUES (%s,%s)" % (date,TEMP)

# write temperature into the database
DB.WriteData(Command)
DB.CloseConnection()
