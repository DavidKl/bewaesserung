#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Thu Jan  9 12:26:07 2020

@author: student
"""
import mysql.connector
import mysql

class DBConnect:
          
    try:
        connection = mysql.connector.connect(host="localhost",user="bewaesserer",passwd= "raspberry",db = "Bewaesserung")
    except:
           print ("Keine Verbindung zum Server")
           exit(0)
    cursor = connection.cursor()
    connection.commit()
    
    # read data from database
    def ReadData(self,select):
        self.cursor.execute(select)
        result = self.cursor.fetchall()
        return result 
    # write data into database
    def WriteData(self,SQLCommand):
        try:
            self.cursor.execute(SQLCommand)
            self.connection.commit()
        except:
            print("Daten konnten nicht in die Datenbank eingetragen werden")
    # close database connection    
    def CloseConnection(self):
        self.cursor.close()
        self.connection.close()
    
