from sense_hat import SenseHat
import os
sense = SenseHat()
sense.clear()

# get CPU temperature of RPI3
def measure_temp():
	core_temp = os.popen('vcgencmd measure_temp').readline()
	return(core_temp.replace("temp=","").replace("'C\n",""))

#Show Msg on Display
def msg(modus,colour):
	sense.show_message(modus,text_colour = colour)

#Get the temperature
def GetTemp():
    temp = sense.get_temperature_from_pressure()
    cpu_temp = float(measure_temp())
    temp_callibrated = temp - ((cpu_temp - temp)/5.466)
    return temp_callibrated
